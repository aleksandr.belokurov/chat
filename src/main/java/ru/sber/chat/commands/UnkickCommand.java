package ru.sber.chat.commands;

import ru.sber.chat.datalayer.UserDAO;
import ru.sber.chat.datalayer.dto.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UnkickCommand implements Command{

    private static final String CHAT = "chat";
    private static final String NICK = "nick";
    private static final String ERROR  = "Unkick redirect error";

    private final UserDAO userDAO;

    public UnkickCommand(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
        String nick = req.getParameter(NICK);
        userDAO.unkick(new User(nick, userDAO.getRole(nick)));
        try {
            resp.sendRedirect(CHAT);
        } catch (IOException e) {
            new CommandRuntimeException().goToErrorPage(ERROR, req, resp);
        }
    }

}
