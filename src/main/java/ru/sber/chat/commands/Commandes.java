package ru.sber.chat.commands;

import ru.sber.chat.datalayer.DBType;


public enum Commandes {

    LOGIN {
        @Override
        public Command getCommand() {
            return new LoginCommand(DBType.MYSQL.getDAOFactory().getUserDAO());
        }
    },
    LOGOUT {
        @Override
        public Command getCommand() {
            return new LogoutCommand(DBType.MYSQL.getDAOFactory().getUserDAO());
        }
    },
    KICK {
        @Override
        public Command getCommand() {
            return new KickCommand(DBType.MYSQL.getDAOFactory().getUserDAO());
        }
    },
    UNKICK {
        @Override
        public Command getCommand() {
            return new UnkickCommand(DBType.MYSQL.getDAOFactory().getUserDAO());
        }
    },
    SEND_MESSAGE {
        @Override
        public Command getCommand() {
            return new SendMessageCommand(DBType.MYSQL.getDAOFactory().getMessageDAO());
        }
    },
    GET_MESSAGE {
        @Override
        public Command getCommand() {
            return new GetMessageCommand(DBType.MYSQL.getDAOFactory().getMessageDAO());
        }
    },
    GET_ALL_LOGGED_USER {
        @Override
        public Command getCommand() {
            return new GetAllLoggedUser(DBType.MYSQL.getDAOFactory().getUserDAO());
        }
    },
    GET_ALL_KICKED_USER {
        @Override
        public Command getCommand() {
            return new GetAllKickedUser(DBType.MYSQL.getDAOFactory().getUserDAO());
        }
    },
    BLOCK{
        @Override
        public Command getCommand() {
            return new BlockCommand(DBType.MYSQL.getDAOFactory().getUserDAO());
        }
    }
    ;


    public abstract Command getCommand();
}
