package ru.sber.chat.commands;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CommandRuntimeException extends RuntimeException {
    private static final String ERROR = "error";
    private static final String ERROR_MESSAGE = "error! go to home.";
    private static final String ERROR_JSP = "/WEB-INF/pages/error.jsp";

    public CommandRuntimeException() {

    }

    public CommandRuntimeException(Throwable throwable) {
        super(throwable);
    }

    public CommandRuntimeException(String message, Throwable throwable) {
        super(message, throwable);
    }


    public void goToErrorPage(Exception exception, HttpServletRequest req, HttpServletResponse resp) {
        try {
            resp.addHeader(ERROR, ERROR_MESSAGE);
            req.setAttribute(ERROR, ERROR_MESSAGE);
            req.getRequestDispatcher(ERROR_JSP).forward(req, resp);
        } catch (ServletException | IOException e) {
            throw new CommandRuntimeException(e);
        }
    }

    public void goToErrorPage(String str, HttpServletRequest req, HttpServletResponse resp) {
        try {
            resp.addHeader(ERROR, str);
            req.setAttribute(ERROR, str);
            req.getRequestDispatcher(ERROR_JSP).forward(req, resp);
        } catch (ServletException | IOException e) {
            throw new CommandRuntimeException(e);
        }
    }

}

