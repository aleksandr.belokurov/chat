package ru.sber.chat.commands;

import ru.sber.chat.datalayer.MessageDAO;
import ru.sber.chat.datalayer.dto.Message;
import ru.sber.chat.datalayer.dto.Status;
import ru.sber.chat.datalayer.dto.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;

public class SendMessageCommand implements Command{

    private static final String CHAT_WAR = "/chat_war/";
    private static final String ERROR  = "SendMessage redirect error";
    private static final String USER = "User";
    private static final String CONTENT = "messageContent";

    private MessageDAO messageDAO;

    public SendMessageCommand(MessageDAO messageDAO) {
        this.messageDAO = messageDAO;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
        String messageContent = req.getParameter(CONTENT);
        if (!messageContent.isEmpty()) {
            messageDAO.sendMessage(
                    new Message((User) req.getSession().getAttribute(USER),
                            LocalDateTime.now(), messageContent,
                            Status.MESSAGE));
        }
        try {
            resp.sendRedirect(CHAT_WAR);
        } catch (IOException e) {
            new CommandRuntimeException().goToErrorPage(ERROR, req, resp);
        }

    }
}
