package ru.sber.chat.commands;

import ru.sber.chat.datalayer.UserDAO;
import ru.sber.chat.datalayer.dto.Role;
import ru.sber.chat.datalayer.dto.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class KickCommand implements Command {

    private static final String CHAT = "chat";
    private static final String NICK = "nick";
    private static final String USER = "User";

    private UserDAO userDAO;

    public KickCommand(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
        userDAO.kick((User) req.getSession().getAttribute(USER),
                new User(req.getParameter(NICK), Role.USER));
        try {
            resp.sendRedirect(CHAT);
        } catch (IOException e) {
            new CommandRuntimeException().goToErrorPage(e, req, resp);
        }

    }
}
