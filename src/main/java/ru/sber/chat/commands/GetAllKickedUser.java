package ru.sber.chat.commands;

import ru.sber.chat.datalayer.UserDAO;
import ru.sber.chat.datalayer.dto.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

public class GetAllKickedUser implements Command{
    private UserDAO userDAO;

    public GetAllKickedUser(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
        List<User> userList = new ArrayList<>(userDAO.getAllKicked());
        req.setAttribute("UsersIsKick", userList);
    }
}
