package ru.sber.chat.commands;

import ru.sber.chat.datalayer.UserDAO;
import ru.sber.chat.datalayer.dto.Role;
import ru.sber.chat.datalayer.dto.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Pattern;

public class LoginCommand implements Command {
    private static final String REGULAR_EXPR_FOR_USER_ROLE = ".*@sber.ru";
    private static final String INPUT_NAME = "You didn't name";
    private static final String NAME_OCCUPIED = "A user with this name already exists";
    private static final String NAME_KICK = "A user with this name kicked";
    private static final String CHAT_WAR = "/chat_war/";
    private static final String NAME  = "name";
    private static final String USER = "User";

    private UserDAO userDAO;

    public LoginCommand(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
        try {
            String name = req.getParameter(NAME);
            if (name.isEmpty()) {
                throw new NullPointerException(INPUT_NAME);
            }
            User user = new User(name, getRoleUser(name));
            if (userDAO.isLoggedIn(user)) {
                throw new NullPointerException(NAME_OCCUPIED);
            } else if(userDAO.isKicked(user)) {
                throw new NullPointerException(NAME_KICK);
            }
            else {
                userDAO.login(user);
            }
            req.getSession().setAttribute(USER, user);

            resp.sendRedirect(CHAT_WAR);
        }catch (NullPointerException | IOException e) {
            new CommandRuntimeException().goToErrorPage(e, req, resp);
        }
    }

    private Role getRoleUser(String name) {
        Role role = Role.USER;
        boolean userIsAdmin = Pattern.matches(REGULAR_EXPR_FOR_USER_ROLE,name);
        if (userIsAdmin) {
            role = Role.ADMIN;
        }
        return role;
    }
}
