package ru.sber.chat.commands;

import ru.sber.chat.datalayer.UserDAO;
import ru.sber.chat.datalayer.dto.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

public class GetAllLoggedUser implements Command{
    private UserDAO userDAO;

    public GetAllLoggedUser(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
        List<User> userList = new ArrayList<>(userDAO.getAllLogged());
        req.setAttribute("Users", userList);
    }
}
