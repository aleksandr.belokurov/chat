package ru.sber.chat.commands;

import ru.sber.chat.datalayer.MessageDAO;
import ru.sber.chat.datalayer.dto.Message;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

public class GetMessageCommand implements Command{
    private static final String MESSAGES = "Messages";

    private MessageDAO messageDAO;

    public GetMessageCommand(MessageDAO messageDAO) {
        this.messageDAO = messageDAO;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
        List<Message> lastMessages =
                new ArrayList<>(messageDAO.getLast(20));
        req.setAttribute(MESSAGES, lastMessages);
    }
}
