package ru.sber.chat.commands;

import ru.sber.chat.datalayer.UserDAO;
import ru.sber.chat.datalayer.dto.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LogoutCommand implements Command{

    private static final String USER = "User";
    private static final String LOGIN = "login";

    private UserDAO userDAO;

    public LogoutCommand(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute(USER);
        session.invalidate();
        if (user != null) {
            userDAO.logout(user);
        }
        try {
            resp.sendRedirect(LOGIN);
        } catch (IOException e) {
            new CommandRuntimeException().goToErrorPage(e, req, resp);
        }

    }
}
