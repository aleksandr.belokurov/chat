package ru.sber.chat.datalayer.dto;

public enum Status {
    LOGIN("A user logged in to the chat"),
    MESSAGE("A user left a message"),
    KICK("A user threw out the other user specified in the message field from the chat"),
    LOGOUT("A user left the chat"),
    BLOCK("A user can't send message");

    private String description;

    Status(String description) {
        this.description = description;
    }
}
