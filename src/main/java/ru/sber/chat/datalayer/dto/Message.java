package ru.sber.chat.datalayer.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

/**
 * Represents chat message
 */
public class Message implements Serializable {

    private static final long serialVersionUID = 1L;

    private final User userFrom;
    private final LocalDateTime date;
    private final String message;
    private final Status status;

    public Message(User userFrom, LocalDateTime date, String message, Status status) {
        this.userFrom = userFrom;
        this.date = date;
        this.message = message;
        this.status = status;
    }


    public User getUserFrom() {
        return userFrom;
    }

    public Status getStatus() {
        return status;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Message mess = (Message) obj;
        return Objects.equals(userFrom, mess.userFrom) && Objects.equals(date, mess.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userFrom, date);
    }

    @Override
    public String toString() {
        return "Message{" +
                "userFrom=" + userFrom +
                ", date=" + date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) +
                ", message='" + message + '\'' +
                ", status=" + status +
                '}';
    }
}
