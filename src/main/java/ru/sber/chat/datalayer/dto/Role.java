package ru.sber.chat.datalayer.dto;

public enum Role {
    ADMIN("Administrator role"),
    USER("User role");

    private String description;

    Role(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
