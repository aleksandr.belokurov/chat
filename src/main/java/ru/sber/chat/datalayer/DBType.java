package ru.sber.chat.datalayer;

import ru.sber.chat.connection.db.ConnectionPool;
import ru.sber.chat.datalayer.db.MySqlDAOFactory;
import ru.sber.chat.datalayer.file.FileDAOFactory;
import ru.sber.chat.datalayer.xml.XMLDAOFactory;

/**
 * Type of db
 */
public enum DBType {

    XML {
        @Override
        public DAOFactory getDAOFactory() {
            return new XMLDAOFactory();
        }
    },

    MYSQL {
        @Override
        public DAOFactory getDAOFactory() {
            return new MySqlDAOFactory(ConnectionPool.getInstance());
        }
    },
    FILE {
        @Override
        public DAOFactory getDAOFactory() {
            return new FileDAOFactory();
        }
    };

    /**
     * Get db type by name
     *
     * @param dbType type founded by name
     * @return database type
     */
    public static DBType getTypeByName(String dbType) {
        try {
            return DBType.valueOf(dbType.toUpperCase());
        } catch (Exception e) {
            throw new DBTypeException();
        }
    }

    public abstract DAOFactory getDAOFactory();

}
