package ru.sber.chat.datalayer.xml;

import ru.sber.chat.datalayer.DAOFactory;
import ru.sber.chat.datalayer.MessageDAO;
import ru.sber.chat.datalayer.UserDAO;

public class XMLDAOFactory extends DAOFactory {


    private final XMLReadParser xmlReadParser;
    private final XMLWriteParser xmlWriteParser;


    public XMLDAOFactory() {
        this.xmlReadParser = new XMLReadParser();
        this.xmlWriteParser = new XMLWriteParser();
    }

    @Override
    public MessageDAO getMessageDAO() {
        return new XMLMessageDAO(xmlReadParser, xmlWriteParser);
    }

    @Override
    public UserDAO getUserDAO() {
        return new XMLUserDAO(getMessageDAO(), xmlReadParser, xmlWriteParser);
    }


}
