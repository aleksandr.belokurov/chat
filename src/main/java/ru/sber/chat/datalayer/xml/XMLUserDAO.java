package ru.sber.chat.datalayer.xml;

import ru.sber.chat.datalayer.MessageDAO;
import ru.sber.chat.datalayer.UserDAO;
import ru.sber.chat.datalayer.dto.Message;
import ru.sber.chat.datalayer.dto.Role;
import ru.sber.chat.datalayer.dto.Status;
import ru.sber.chat.datalayer.dto.User;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class XMLUserDAO implements UserDAO {

    private static final String LOGIN_IN_CHAT = " login in chat";
    private static final String LOGOUT_CHAT = " logout chat";

    private final MessageDAO xmlMessageDAO;
    private final XMLReadParser xmlReadParser;
    private final XMLWriteParser xmlWriteParser;

    public XMLUserDAO(MessageDAO xmlMessageDAO, XMLReadParser xmlReadParser, XMLWriteParser xmlWriteParser) {
        this.xmlMessageDAO = xmlMessageDAO;
        this.xmlReadParser = xmlReadParser;
        this.xmlWriteParser = xmlWriteParser;
    }

    /**
     * Login user using xml parser
     *
     * @param userToLogin user we want to login
     */
    @Override
    public void login(User userToLogin) {
        if (!isLoggedIn(userToLogin)) {
            if (xmlReadParser.findUserForXML(userToLogin.getName()) == null) {
                xmlWriteParser.writeUser(userToLogin);
            }
            LocalDateTime localDateTime = LocalDateTime.now();
            Message message = new Message(userToLogin, localDateTime, userToLogin.getName() + LOGIN_IN_CHAT, Status.LOGIN);
            xmlMessageDAO.sendMessage(message);
        }
    }

    /**
     * Check user logged in using parser
     *
     * @param user user to check
     * @return boolean result
     */
    @Override
    public boolean isLoggedIn(User user) {
        List<Message> messageList = xmlReadParser.getLastMessagesFromXML(xmlReadParser.countMessage());
        Collections.reverse(messageList);
        return isLoggedUser(user, messageList);
    }

    /**
     * Logout user using xml file
     *
     * @param userToLogout user we want to logout
     */
    @Override
    public void logout(User userToLogout) {
        if (isLoggedIn(userToLogout)) {
            LocalDateTime localDateTime = LocalDateTime.now();
            Message message = new Message(userToLogout, localDateTime, userToLogout.getName() + LOGOUT_CHAT, Status.LOGOUT);
            xmlMessageDAO.sendMessage(message);
        }
    }

    /**
     * Unckick user using xml parser
     *
     * @param user user we want to logout
     */
    @Override
    public void unkick(User user) {
        if (isKicked(user)) {
            xmlWriteParser.removeMessageAboutKick(user);
        }
    }


    /**
     * @param admin        - user responsible for the kick action (with the role admin)
     * @param kickableUser - user that should be kicked
     */
    @Override
    public void kick(User admin, User kickableUser) {
        if (!isKicked(kickableUser)) {
            LocalDateTime localDateTime = LocalDateTime.now();
            Message message = new Message(admin, localDateTime, kickableUser.getName(), Status.KICK);
            xmlMessageDAO.sendMessage(message);
            logout(kickableUser);
        }
    }

    /**
     * Check user is kicked using xml parser
     *
     * @param user user to check
     * @return boolean result
     */
    @Override
    public boolean isKicked(User user) {
        List<Message> messageList = xmlReadParser.getLastMessagesFromXML(xmlReadParser.countMessage());
        return isUser(user, messageList, Status.KICK);
    }

    @Override
    public void block(User admin, User blockedUser) {
        if (!isKicked(blockedUser)) {
            LocalDateTime localDateTime = LocalDateTime.now();
            Message message = new Message(admin, localDateTime, blockedUser.getName(), Status.BLOCK);
            xmlMessageDAO.sendMessage(message);
            logout(blockedUser);
        }
    }

    @Override
    public boolean isBlocked(User user) {
        List<Message> messageList = xmlReadParser.getLastMessagesFromXML(xmlReadParser.countMessage());
        return isUser(user, messageList, Status.BLOCK);
    }

    /**
     * Get all logged users using xml parser
     *
     * @return all logged users from xml file
     */
    @Override
    public List<User> getAllLogged() {
        List<Message> messageList = xmlReadParser.getLastMessagesFromXML(xmlReadParser.countMessage());
        Collections.reverse(messageList);
        return xmlReadParser.getUsersFromXML().stream().filter(user -> isLoggedUser(user, messageList)).collect(Collectors.toList());
    }

    /**
     * Get all kicked users
     *
     * @return all kicked users from xml file
     */
    @Override
    public List<User> getAllKicked() {
        List<Message> messageList = xmlReadParser.getLastMessagesFromXML(xmlReadParser.countMessage());
        Collections.reverse(messageList);
        return xmlReadParser.getUsersFromXML().stream().filter(user -> isUser(user, messageList, Status.KICK)).collect(Collectors.toList());
    }

    /**
     * Get role from xml file using parser by user nick
     *
     * @param nick nick of user to find the role
     * @return user role
     */
    @Override
    public Role getRole(String nick) throws NoSuchElementException {
        return xmlReadParser.getUsersFromXML().stream().filter(user -> user.getName().equals(nick)).
                findFirst().orElseThrow(NoSuchElementException::new).getRole();

    }

    private boolean isLoggedUser(User user, List<Message> messageList) {
        boolean result = false;
        for (Message message : messageList) {
            if (message.getUserFrom().equals(user) && (message.getStatus() != Status.LOGOUT || message.getStatus() != Status.KICK)) {
                result = true;
                break;
            } else {
                break;
            }
        }
        return result;
    }
    private boolean isUser(User user, List<Message> messageList, Status status) {
        boolean result = false;
        for (Message message : messageList) {
            if (message.getMessage().equals(user.getName()) && message.getStatus() == status) {
                result = true;
                break;
            }
        }
        return result;
    }

}
