package ru.sber.chat.datalayer.xml;

import ru.sber.chat.datalayer.MessageDAO;
import ru.sber.chat.datalayer.dto.Message;

import java.util.List;

public class XMLMessageDAO implements MessageDAO {

    private final XMLReadParser xmlReadParser;
    private final XMLWriteParser xmlWriteParser;


    public XMLMessageDAO(XMLReadParser xmlReadParser, XMLWriteParser xmlWriteParser) {
        this.xmlReadParser = xmlReadParser;
        this.xmlWriteParser = xmlWriteParser;
    }

    /**
     * Get last messages from xml file using parser
     */
    @Override
    public List<Message> getLast(int count) {
        return xmlReadParser.getLastMessagesFromXML(count);
    }

    /**
     * Send message using xml file and parse
     *
     * @param message - message to send
     */
    @Override
    public void sendMessage(Message message) {
        xmlWriteParser.writeMessageForXML(message);
    }

}
