package ru.sber.chat.datalayer.xml;

import ru.sber.chat.datalayer.dto.Message;
import ru.sber.chat.datalayer.dto.Role;
import ru.sber.chat.datalayer.dto.Status;
import ru.sber.chat.datalayer.dto.User;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public final class XMLReadParser {

    private static final int FIRST_ELEMENT_NODE_LIST = 1;
    private static final int SECOND_ELEMENT_NODE_LIST = 3;
    private static final int THIRD_ELEMENT_NODE_LIST = 5;
    private static final int FOURTH_ELEMENT_NODE_LIST = 7;
    private static final String XML_TAG_NAME_MESSAGE = "chat:message";
    private static final String XML_TAG_NAME_USER = "chat:user";
    private static final String PATH_XML = "src/main/resources/xml/online_chat.xml";


    public List<Message> getLastMessagesFromXML(int count) {
        List<Message> messageList = new ArrayList<>(count);
        Document document = getXMLDocument();
        NodeList messages = document.getElementsByTagName(XML_TAG_NAME_MESSAGE);
        int i = messages.getLength() - count;
        if (i < 0) {
            i = 0;
        }
        for (; i < messages.getLength(); i++) {
            NodeList message = messages.item(i).getChildNodes();
            User user = findUserForXML(message.item(FIRST_ELEMENT_NODE_LIST).getTextContent());
            messageList.add(new Message(
                    user,
                    LocalDateTime.parse(message.item(SECOND_ELEMENT_NODE_LIST).getTextContent()),
                    message.item(THIRD_ELEMENT_NODE_LIST).getTextContent(),
                    Status.valueOf(message.item(FOURTH_ELEMENT_NODE_LIST).getTextContent())
            ));
        }
        return messageList;
    }

    public int countMessage() {
        Document document = getXMLDocument();
        NodeList messages = document.getElementsByTagName(XML_TAG_NAME_MESSAGE);
        return messages.getLength();
    }

    public List<User> getUsersFromXML() {
        List<User> userList = new ArrayList<>();
        Document document = getXMLDocument();
        NodeList messages = document.getElementsByTagName(XML_TAG_NAME_USER);
        for (int i = 0; i < messages.getLength(); i++) {
            NodeList user = messages.item(i).getChildNodes();
            userList.add(new User(
                    user.item(FIRST_ELEMENT_NODE_LIST).getTextContent(),
                    Role.valueOf(user.item(SECOND_ELEMENT_NODE_LIST).getTextContent())
            ));
        }
        return userList;
    }

    public User findUserForXML(String name) {
        List<User> userList = getUsersFromXML();
        User result = null;
        for (User user : userList) {
            if (user.getName().equals(name)) {
                result = user;
            }
        }
        return result;
    }

    private Document getXMLDocument() throws XMLParseException {
        Document document = null;
        try {
            DocumentBuilderFactory documentBuilder = DocumentBuilderFactory.newInstance();
            documentBuilder.setAttribute(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            documentBuilder.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
            documentBuilder.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
            document = documentBuilder.newDocumentBuilder().parse(new File(PATH_XML));
            if (document == null) {
                throw new NullPointerException();
            }
        } catch (ParserConfigurationException | IOException | SAXException | NullPointerException exception) {
            throw new XMLParseException(exception);
        }
        return document;
    }

}
