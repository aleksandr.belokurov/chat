package ru.sber.chat.datalayer.xml;

import ru.sber.chat.datalayer.dto.Message;
import ru.sber.chat.datalayer.dto.Status;
import ru.sber.chat.datalayer.dto.User;

import org.jdom2.*;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import javax.xml.XMLConstants;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class XMLWriteParser {

    private static final String URI = "http://www.epam.com/chat";
    private static final String PATH_XML = "src/main/resources/xml/online_chat.xml";
    private static final String WORD_CHAT = "chat";
    private static final String WORD_MESSAGE = "message";
    private static final String WORD_MESSAGES = "messages";
    private static final String WORD_USER_FROM = "user_from";
    private static final String WORD_TIME_STAMP = "time_stamp";
    private static final String WORD_TEXT = "text";
    private static final String WORD_MESSAGE_STATUS = "message_status";


    private static final String WORD_USERS = "users";
    private static final String WORD_USER = "user";
    private static final String WORD_LOGIN = "login";
    private static final String WORD_ROLE_USER = "role_user";

    public void writeMessageForXML(Message message) {
        try {
            Document document = getDocumentForXML();
            Element messages = document.getRootElement().getChild(WORD_MESSAGES, Namespace.getNamespace(URI));
            Element newMessage = new Element(WORD_MESSAGE, WORD_CHAT, URI);
            newMessage.addContent(new Element(WORD_USER_FROM, WORD_CHAT, URI).addContent(message.getUserFrom().getName()));
            newMessage.addContent(new Element(WORD_TIME_STAMP, WORD_CHAT, URI).addContent(message.getDate().toString()));
            newMessage.addContent(new Element(WORD_TEXT, WORD_CHAT, URI).addContent(message.getMessage()));
            newMessage.addContent(new Element(WORD_MESSAGE_STATUS, WORD_CHAT, URI).addContent(message.getStatus().name()));
            messages.addContent(newMessage);
            XMLOutputter xmlWriter = new XMLOutputter(Format.getPrettyFormat());
            xmlWriter.output(document, new FileOutputStream(PATH_XML));
            System.out.flush();
        } catch (IOException e) {
            throw new XMLParseException(e);
        }
    }

    public void writeUser(User user) {
        try {
            Document document = getDocumentForXML();
            Element users = document.getRootElement().getChild(WORD_USERS, Namespace.getNamespace(URI));
            Element newUser = new Element(WORD_USER, WORD_CHAT, URI);
            newUser.addContent(new Element(WORD_LOGIN, WORD_CHAT, URI).addContent(user.getName()));
            newUser.addContent(new Element(WORD_ROLE_USER, WORD_CHAT, URI).addContent(user.getRole().name()));
            users.addContent(newUser);
            XMLOutputter xmlWriter = new XMLOutputter(Format.getPrettyFormat());
            xmlWriter.output(document, new FileOutputStream(PATH_XML));
            System.out.flush();
        } catch (IOException e) {
            throw new XMLParseException(e);
        }
    }

    public void removeMessageAboutKick(User user) {
        try {
            Document document = getDocumentForXML();
            Element messages = document.getRootElement().getChild(WORD_MESSAGES, Namespace.getNamespace(URI));
            List<Element> messageList = messages.getChildren(WORD_MESSAGE, Namespace.getNamespace(URI));
            for (Element message : messageList) {
                if (message.getChildText(WORD_TEXT, Namespace.getNamespace(URI)).equals(user.getName()) &&
                        message.getChildText(WORD_MESSAGE_STATUS, Namespace.getNamespace(URI)).equals(Status.KICK.name())) {
                    messages.removeContent(message);
                }
            }
            XMLOutputter xmlWriter = new XMLOutputter(Format.getPrettyFormat());
            xmlWriter.output(document, new FileOutputStream(PATH_XML));
            System.out.flush();
        } catch (IOException e) {
            throw new XMLParseException(e);
        }
    }

    private Document getDocumentForXML() {
        Document document = null;
        try {
            SAXBuilder builder = new SAXBuilder();
            builder.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
            builder.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
            document = builder.build(PATH_XML);
            if (document == null) {
                throw new NullPointerException();
            }
        } catch (IOException | JDOMException | NullPointerException e) {
            throw new XMLParseException(e);
        }
        return document;
    }

}
