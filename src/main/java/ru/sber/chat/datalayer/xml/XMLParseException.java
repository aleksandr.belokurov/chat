package ru.sber.chat.datalayer.xml;

public class XMLParseException extends RuntimeException {

    private static final String UNABLE_WRITE_IN_FILE = "unable use xml file";

    public XMLParseException() {
        super(UNABLE_WRITE_IN_FILE);
    }

    public XMLParseException(String message) {
        super(message);
    }

    public XMLParseException(Throwable throwable) {
        super(UNABLE_WRITE_IN_FILE, throwable);
    }

    public XMLParseException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
