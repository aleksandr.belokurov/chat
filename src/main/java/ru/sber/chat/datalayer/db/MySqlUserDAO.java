package ru.sber.chat.datalayer.db;

import ru.sber.chat.connection.db.ConnectionPool;
import ru.sber.chat.datalayer.MessageDAO;
import ru.sber.chat.datalayer.UserDAO;
import ru.sber.chat.datalayer.dto.Message;
import ru.sber.chat.datalayer.dto.Role;
import ru.sber.chat.datalayer.dto.Status;
import ru.sber.chat.datalayer.dto.User;
import org.apache.log4j.Logger;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class MySqlUserDAO implements UserDAO {

    private static final Logger LOGGER = Logger.getLogger(MySqlUserDAO.class);
    private static final String KEY_RESOURCE_FOR_CHECK_LOGGED = "query.check.logged.user";
    private static final String KEY_RESOURCE_FOR_CHECK_KICKED = "query.check.kicked.user";
    private static final String KEY_RESOURCE_FOR_CHECK_BLOCKED= "query.check.blocked.user";
    private static final String KEY_RESOURCE_FOR_UNKCIK = "query.delete.message.about.kick";
    private static final String KEY_RESOURCE_FOR_ROLE = "query.get.role.user";
    private static final String KEY_RESOURCE_FOR_ALL_LOGGED = "query.all.logged.user";
    private static final String KEY_RESOURCE_FOR_ALL_KICKED = "query.all.kicked.user";
    private static final String KEY_RESOURCE_FOR_CHECK_USER_CREATED = "query.check.user.created";
    private static final String KEY_RESOURCE_FOR_CREATE_USER = "query.add.user";

    private static final String LOGOUT_CHAT = "user logout chat";
    private static final String LOGIN_IN_CHAT = "user login in chat";
    private static final String DONT_ACCESS = "you don't have access";

    private static final int FIRST_PARAM = 1;
    private static final int SECOND_PARAM = 2;


    private final MessageDAO messageDAO;
    private final ConnectionPool connectionPool;
    private final ResourceBundle resourceBundle;

    public MySqlUserDAO(MessageDAO messageDAO, ConnectionPool connectionPool, ResourceBundle resourceBundle) {
        this.messageDAO = messageDAO;
        this.connectionPool = connectionPool;
        this.resourceBundle = resourceBundle;
    }


    /**
     * @param userToLogin user we want to login
     */
    @Override
    public void login(User userToLogin) {
        if (!basicCheckQueryForUser(userToLogin, KEY_RESOURCE_FOR_CHECK_USER_CREATED)) {
            createUser(userToLogin, KEY_RESOURCE_FOR_CREATE_USER);
        }
        Message message = new Message(userToLogin, LocalDateTime.now(), LOGIN_IN_CHAT, Status.LOGIN);
        messageDAO.sendMessage(message);
    }

    /**
     * @param user user to check
     * @return boolean is user logged
     */
    @Override
    public boolean isLoggedIn(User user) {
        return basicCheckQueryForUserTwoParams(user, KEY_RESOURCE_FOR_CHECK_LOGGED);
    }

    /**
     * @param userToLogout user we want to logout
     */
    @Override
    public void logout(User userToLogout) {
        if (isLoggedIn(userToLogout)) {
            Message message = new Message(userToLogout, LocalDateTime.now(), LOGOUT_CHAT, Status.LOGOUT);
            messageDAO.sendMessage(message);
        }
    }

    @Override
    public void unkick(User user) {
        if (isKicked(user)) {
            PreparedStatement preparedStatement = null;
            Connection connection = null;
            try {
                connection = connectionPool.getConnection();
                preparedStatement = connection.prepareStatement(resourceBundle.
                        getString(KEY_RESOURCE_FOR_UNKCIK));
                preparedStatement.setString(FIRST_PARAM, user.getName());
                preparedStatement.executeUpdate();

            } catch (SQLException e) {
                throw new MySqlRuntimeException(e.getMessage(), e);
            } finally {
                connectionPool.closeConnection(connection, preparedStatement);
            }
        }
    }

    /**
     * @param admin        - user responsible for the kick action (with the role admin)
     * @param kickableUser - user that should be kicked
     */
    @Override
    public void kick(User admin, User kickableUser) {
        if (admin.getRole() == Role.ADMIN) {
            Connection connection = connectionPool.getConnection();
            try {
                connection.setAutoCommit(false);
                MySqlMessageDAO mySqlMessageDAO = (MySqlMessageDAO) messageDAO;
                mySqlMessageDAO.sendManyMessage(connection, new Message(admin, LocalDateTime.now(), kickableUser.getName(), Status.KICK));
                if (isLoggedIn(kickableUser)) {
                    mySqlMessageDAO.sendManyMessage(connection, new Message(kickableUser, LocalDateTime.now(), LOGOUT_CHAT, Status.LOGOUT));
                }
                connection.commit();
            } catch (SQLException e) {
                doRollback(connection);
                throw new MySqlRuntimeException(e.getMessage(), e);
            } finally {
                connectionPool.closeConnection(connection);
            }

        } else {
            LOGGER.info(DONT_ACCESS);
        }
    }


    /**
     * @param user user to check
     * @return boolean is kicked
     */
    @Override
    public boolean isKicked(User user) {
        return basicCheckQueryForUser(user, KEY_RESOURCE_FOR_CHECK_KICKED);
    }

    @Override
    public void block(User admin, User blockedUser) {
        if (admin.getRole() == Role.ADMIN) {
            Connection connection = connectionPool.getConnection();
            try {
                connection.setAutoCommit(false);
                MySqlMessageDAO mySqlMessageDAO = (MySqlMessageDAO) messageDAO;
                mySqlMessageDAO.sendManyMessage(connection, new Message(admin, LocalDateTime.now(), blockedUser.getName(), Status.BLOCK));
                connection.commit();
            } catch (SQLException e) {
                doRollback(connection);
                throw new MySqlRuntimeException(e.getMessage(), e);
            } finally {
                connectionPool.closeConnection(connection);
            }

        } else {
            LOGGER.info(DONT_ACCESS);
        }
    }

    @Override
    public boolean isBlocked(User user) {
        return basicCheckQueryForUser(user, KEY_RESOURCE_FOR_CHECK_BLOCKED);
    }


    /**
     * @return List<User> AllLogged
     */
    @Override
    public List<User> getAllLogged() {
        return basicQueryAllUser(KEY_RESOURCE_FOR_ALL_LOGGED);
    }

    /**
     * @return List<User> AllKicked() {
     */
    @Override
    public List<User> getAllKicked() {
        return basicQueryAllUser(KEY_RESOURCE_FOR_ALL_KICKED);
    }

    /**
     * @param nick nick of user to find the role
     * @return Role role
     */
    @Override
    public Role getRole(String nick) {
        Optional<Role> role = Optional.empty();
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(resourceBundle.
                    getString(KEY_RESOURCE_FOR_ROLE));
            preparedStatement.setString(FIRST_PARAM, nick);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            role = Optional.of(Role.valueOf(resultSet.getString(FIRST_PARAM)));

        } catch (SQLException e) {
            throw new MySqlRuntimeException(e.getMessage(), e);
        } finally {
            connectionPool.closeConnection(connection, preparedStatement);
        }
        return role.orElse(Role.USER);
    }

    private void createUser(User user, String keyQuery) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(resourceBundle.
                    getString(keyQuery));
            preparedStatement.setString(FIRST_PARAM, user.getName());
            preparedStatement.setString(SECOND_PARAM, user.getRole().toString());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new MySqlRuntimeException(e.getMessage(), e);
        } finally {
            connectionPool.closeConnection(connection, preparedStatement);
        }
    }

    private boolean basicCheckQueryForUser(User user, String keyQuery) {
        boolean result = false;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(resourceBundle.
                    getString(keyQuery));
            preparedStatement.setString(FIRST_PARAM, user.getName());
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            result = resultSet.getBoolean(FIRST_PARAM);

        } catch (SQLException e) {
            throw new MySqlRuntimeException(e.getMessage(), e);
        } finally {
            connectionPool.closeConnection(connection, preparedStatement);
        }
        return result;
    }

    private boolean basicCheckQueryForUserTwoParams(User user, String keyQuery) {
        boolean result = false;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(resourceBundle.getString(keyQuery));
            preparedStatement.setString(FIRST_PARAM, user.getName());
            preparedStatement.setString(SECOND_PARAM, user.getName());
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            result = resultSet.getBoolean(FIRST_PARAM);
        } catch (SQLException e) {
            throw new MySqlRuntimeException(e.getMessage(), e);
        } finally {
            connectionPool.closeConnection(connection, preparedStatement);
        }
        return result;
    }

    private List<User> basicQueryAllUser(String keyQuery) {
        List<User> userList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(resourceBundle.getString(keyQuery));
            ResultSet resultSet = preparedStatement.executeQuery(resourceBundle.
                    getString(keyQuery));
            while (resultSet.next()) {
                userList.add(createUserWithResultSet(resultSet));
            }
        } catch (SQLException e) {
            throw new MySqlRuntimeException(e.getMessage(), e);
        } finally {
            connectionPool.closeConnection(connection, preparedStatement);
        }
        return userList;
    }

    private User createUserWithResultSet(ResultSet resultSet) {
        try {
            return new User(resultSet.getString(FIRST_PARAM), Role.valueOf(resultSet.getString(SECOND_PARAM)));
        } catch (SQLException e) {
            throw new MySqlRuntimeException(e.getMessage(), e);
        }
    }

    private void doRollback(Connection c) {
        try {
            c.rollback();
        } catch (SQLException ex) {
            throw new MySqlRuntimeException();
        }
    }
}
