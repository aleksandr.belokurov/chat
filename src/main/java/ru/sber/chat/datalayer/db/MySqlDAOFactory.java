package ru.sber.chat.datalayer.db;

import ru.sber.chat.connection.db.ConnectionPool;
import ru.sber.chat.datalayer.DAOFactory;
import ru.sber.chat.datalayer.MessageDAO;
import ru.sber.chat.datalayer.UserDAO;

import java.util.ResourceBundle;

/**
 *
 */
public class MySqlDAOFactory extends DAOFactory {

    private static final String QUERY_RESOURCE = "queries";

    private final ConnectionPool connectionPool;

    public MySqlDAOFactory(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }

    @Override
    public MessageDAO getMessageDAO() {
        return new MySqlMessageDAO(connectionPool, ResourceBundle.getBundle(QUERY_RESOURCE));
    }

    @Override
    public UserDAO getUserDAO() {
        return new MySqlUserDAO(getMessageDAO(), connectionPool, ResourceBundle.getBundle(QUERY_RESOURCE));
    }


}
