package ru.sber.chat.datalayer.db;


public class MySqlRuntimeException extends RuntimeException {

    private static final String UNABLE_USE_MYSQL = "unable use MySql";

    public MySqlRuntimeException() {
        super(UNABLE_USE_MYSQL);
    }
    public MySqlRuntimeException(Throwable throwable) {
        super(UNABLE_USE_MYSQL, throwable);
    }

    public MySqlRuntimeException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
