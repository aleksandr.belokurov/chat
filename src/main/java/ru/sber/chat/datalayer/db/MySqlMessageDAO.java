package ru.sber.chat.datalayer.db;

import ru.sber.chat.connection.db.ConnectionPool;
import ru.sber.chat.datalayer.MessageDAO;
import ru.sber.chat.datalayer.dto.Message;
import ru.sber.chat.datalayer.dto.Role;
import ru.sber.chat.datalayer.dto.Status;
import ru.sber.chat.datalayer.dto.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class MySqlMessageDAO implements MessageDAO {
    private static final String KEY_RESOURCE_FOR_SEND_MESSAGE = "query.create.message";
    private static final String KEY_RESOURCE_FOR_GET_MESSAGE = "query.get.message";
    private static final int FIRST_PARAM = 1;
    private static final int SECOND_PARAM = 2;
    private static final int THIRD_PARAM = 3;
    private static final int FOURTH_PARAM = 4;
    private static final int FIFTH_PARAM = 5;

    private final ConnectionPool connectionPool;
    private final ResourceBundle resourceBundle;

    public MySqlMessageDAO(ConnectionPool connectionPool, ResourceBundle resourceBundle) {
        this.connectionPool = connectionPool;
        this.resourceBundle = resourceBundle;
    }

    @Override
    public void sendMessage(Message message) {
        sendMessage(connectionPool.getConnection(), message);
    }

    @Override
    public List<Message> getLast(int count) {
        List<Message> messageList = new ArrayList<>(count);
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(resourceBundle.
                    getString(KEY_RESOURCE_FOR_GET_MESSAGE));
            preparedStatement.setInt(FIRST_PARAM, count);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                messageList.add(createMessageFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            throw new MySqlRuntimeException(e.getMessage(), e);
        } finally {
            connectionPool.closeConnection(connection, preparedStatement, resultSet);
        }
        return messageList;
    }

    private Message createMessageFromResultSet(ResultSet resultSet) {
        Optional<Message> message;
        try {
            User user = new User(resultSet.getString(FIRST_PARAM), Role.valueOf(resultSet.getString(FIFTH_PARAM)));
            LocalDateTime localDateTime = resultSet.getTimestamp(SECOND_PARAM).toLocalDateTime();
            message = Optional.of(new Message(user, localDateTime,
                    resultSet.getString(THIRD_PARAM), Status.valueOf(resultSet.getString(FOURTH_PARAM))));
        } catch (SQLException e) {
            throw new MySqlRuntimeException(e.getMessage(), e);
        }
        return message.orElseThrow(MySqlRuntimeException::new);
    }

    public void sendMessage(Connection connection, Message message) {
        PreparedStatement preparedStatement = null;
        try{
            preparedStatement = connection.prepareStatement(resourceBundle.getString(KEY_RESOURCE_FOR_SEND_MESSAGE));
            preparedStatement.setString(FIRST_PARAM, message.getUserFrom().getName());
            preparedStatement.setString(SECOND_PARAM, message.getMessage());
            preparedStatement.setString(THIRD_PARAM, message.getDate().toString());
            preparedStatement.setString(FOURTH_PARAM, message.getStatus().name());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new MySqlRuntimeException(e.getMessage(), e);
        } finally {
            connectionPool.closeConnection(connection , preparedStatement);
        }
    }

    public void sendManyMessage(Connection connection, Message message) throws SQLException {
        PreparedStatement preparedStatement = null;
        try{
            preparedStatement = connection.prepareStatement(resourceBundle.getString(KEY_RESOURCE_FOR_SEND_MESSAGE));
            preparedStatement.setString(FIRST_PARAM, message.getUserFrom().getName());
            preparedStatement.setString(SECOND_PARAM, message.getMessage());
            preparedStatement.setString(THIRD_PARAM, message.getDate().toString());
            preparedStatement.setString(FOURTH_PARAM, message.getStatus().name());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new MySqlRuntimeException(e.getMessage(), e);
        } finally {
            preparedStatement.close();
        }
    }

}

