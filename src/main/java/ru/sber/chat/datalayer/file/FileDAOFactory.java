package ru.sber.chat.datalayer.file;

import ru.sber.chat.datalayer.DAOFactory;
import ru.sber.chat.datalayer.MessageDAO;
import ru.sber.chat.datalayer.UserDAO;

public class FileDAOFactory extends DAOFactory {



    @Override
    public MessageDAO getMessageDAO() {
        return new FileMessageDAO();
    }

    @Override
    public UserDAO getUserDAO() {
        return new FileUserDAO();
    }
}
