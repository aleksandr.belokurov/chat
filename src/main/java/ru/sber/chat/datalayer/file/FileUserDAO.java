package ru.sber.chat.datalayer.file;

import ru.sber.chat.datalayer.UserDAO;
import ru.sber.chat.datalayer.dto.Message;
import ru.sber.chat.datalayer.dto.Role;
import ru.sber.chat.datalayer.dto.Status;
import ru.sber.chat.datalayer.dto.User;

import java.io.*;
import java.time.LocalDateTime;
import java.util.*;

public class FileUserDAO implements UserDAO {

    private static final String PATH = "src/main/resources/file/message.txt";

    @Override
    public void login(User userToLogin) {
        List<Message> list = readList();
        Message message = new Message(userToLogin, LocalDateTime.now(), "Login", Status.LOGIN);
        list.add(message);
        try(FileOutputStream outputStream = new FileOutputStream(PATH);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)) {
            objectOutputStream.writeObject(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isLoggedIn(User user) {
        List<Message> list = readList();
        boolean result = true;
        for (Message message : list) {
            if (message.getUserFrom().equals(user)) {
                if (!message.getStatus().equals(Status.LOGOUT) && !message.getStatus().equals(Status.KICK)) {
                    result = false;
                }
            }
            break;
        }
        return result;
    }

    @Override
    public void logout(User userToLogout) {
        List<Message> list = readList();
        Message message = new Message(userToLogout, LocalDateTime.now(), "Login", Status.LOGOUT);
        list.add(message);
        try(FileOutputStream outputStream = new FileOutputStream(PATH);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)) {
            objectOutputStream.writeObject(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void unkick(User user) {
        List<Message> list = readList();
        Iterator<Message> iterator = list.listIterator();
        while(iterator.hasNext()){
            Message message = iterator.next();
            if (message.getStatus().equals(Status.KICK) && message.getMessage().equals(user.getName())) {
                iterator.remove();
            }
        }
        try(FileOutputStream outputStream = new FileOutputStream(PATH);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)) {
            objectOutputStream.writeObject(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void kick(User admin, User kickableUser) {
        List<Message> list = readList();
        Message message = new Message(admin, LocalDateTime.now(), kickableUser.getName(), Status.KICK);
        list.add(message);
        try(FileOutputStream outputStream = new FileOutputStream(PATH);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)) {
            objectOutputStream.writeObject(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isKicked(User user) {
        List<Message> list = readList();
        boolean result = false;
        for (Message message : list) {
            if (message.getMessage().equalsIgnoreCase(user.getName())) {
                if (message.getStatus().equals(Status.KICK)) {
                    result = true;
                    break;
                }
            }

        }
        return result;
    }

    @Override
    public void block(User admin, User blockedUser) {
        List<Message> list = readList();
        Message message = new Message(admin, LocalDateTime.now(), blockedUser.getName(), Status.BLOCK);
        list.add(message);
        try(FileOutputStream outputStream = new FileOutputStream(PATH);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)) {
            objectOutputStream.writeObject(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isBlocked(User user) {
        List<Message> list = readList();
        boolean result = false;
        for (Message message : list) {
            if (message.getMessage().equalsIgnoreCase(user.getName())) {
                if (message.getStatus().equals(Status.BLOCK)) {
                    result = true;
                    break;
                }
            }

        }
        return result;
    }

    @Override
    public List<User> getAllLogged() {
        List<Message> list = readList();
        Set<User> result = new HashSet<>();
        list.forEach((e) -> {
            if (isLoggedIn(e.getUserFrom())) {
                result.add(e.getUserFrom());
            }
        });
        return new ArrayList<>(result);

    }

    @Override
    public List<User> getAllKicked() {
        List<Message> list = readList();
        Set<User> result = new HashSet<>();
        for (Message message : list) {
            User user = new User(message.getMessage(), Role.USER);
            if (isKicked(user)) {
                result.add(user);
            }
        }
        return new ArrayList<>(result);
    }

    @Override
    public Role getRole(String nick) {
        List<Message> list = readList();
        Role role = Role.USER;
        for (Message message : list) {
            if (message.getUserFrom().getName().equals(nick)) {
                role = message.getUserFrom().getRole();
            }
        }
        return role;
    }

    public List<Message> readList() {
        List<Message> list = new ArrayList<>();
        try(FileInputStream fileInputStream = new FileInputStream(PATH);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
            list = (List<Message>) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }
}
