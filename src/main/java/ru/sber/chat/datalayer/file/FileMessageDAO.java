package ru.sber.chat.datalayer.file;

import ru.sber.chat.datalayer.MessageDAO;
import ru.sber.chat.datalayer.dto.Message;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileMessageDAO implements MessageDAO {

    private static final String PATH = "src/main/resources/file/message.txt";

    @Override
    public void sendMessage(Message message) {
        List<Message> list = readList();
        list.add(message);
        try(FileOutputStream outputStream = new FileOutputStream(PATH);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)) {
            objectOutputStream.writeObject(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Message> getLast(int count) {
        List<Message> list = readList();
        int i = list.size() - count;
        if (i < 0) {
            i = 0;
        }
        return list.subList(i, list.size());
    }

    public List<Message> readList() {
        List<Message> list = new ArrayList<>();
        try(FileInputStream fileInputStream = new FileInputStream(PATH);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
            list = (List<Message>) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }
}
