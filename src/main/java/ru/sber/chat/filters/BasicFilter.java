package ru.sber.chat.filters;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ResourceBundle;

public class BasicFilter implements Filter {

    private static final Logger logger = Logger.getLogger(BasicFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

        ResourceBundle resourceBundle = ResourceBundle.getBundle("infoOfProject");
        ResourceBundle resourceBundleGit = ResourceBundle.getBundle("git");
        logger.info(resourceBundle.getString("groupId"));
        logger.info(resourceBundle.getString("artifactId"));
        logger.info(resourceBundle.getString("version"));
        logger.info("Git info : ");
        logger.info(resourceBundleGit.getString("git.commit.user.name"));
        logger.info(resourceBundleGit.getString("git.commit.id"));
        logger.info(resourceBundleGit.getString("git.commit.message.full"));
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        servletResponse.setContentType("text/html;charset=UTF-8");
        servletRequest.setCharacterEncoding("UTF-8");
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
