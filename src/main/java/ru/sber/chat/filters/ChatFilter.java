package ru.sber.chat.filters;

import ru.sber.chat.datalayer.DAOFactory;
import ru.sber.chat.datalayer.DBType;
import ru.sber.chat.datalayer.UserDAO;
import ru.sber.chat.datalayer.dto.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Implementation of servlet filter
 */
public class ChatFilter implements Filter {

    private static final String CMD = "cmd";
    private static final String LOGIN = "login";
    private static final String REDIRECT = "redirect";
    private static final String USER = "User";
    private static final String CHAT = "chat";


    private UserDAO userDAO;

    @Override
    public void destroy() {
        //
    }

    /**
     * Applying filter to request
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        User user = (User) req.getSession().getAttribute(USER);
        if (req.getServletContext().getAttribute(CMD) == null) {
            if (user == null) {
                req.setAttribute(REDIRECT, LOGIN);
            } else {
                if (userDAO.isKicked(user)) {
                    req.getSession().invalidate();
                }
                req.setAttribute(REDIRECT, CHAT);
            }
        }
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
        DAOFactory daoFactory = DBType.MYSQL.getDAOFactory();
        userDAO = daoFactory.getUserDAO();
    }

}
