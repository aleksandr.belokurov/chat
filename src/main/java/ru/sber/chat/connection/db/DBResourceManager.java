package ru.sber.chat.connection.db;

import java.util.ResourceBundle;

public class DBResourceManager {
    private static final String NAME_DB_RESOURCE = "db";
    private static final DBResourceManager INSTANCE = new DBResourceManager();

    private final ResourceBundle bundle =
            ResourceBundle.getBundle(NAME_DB_RESOURCE);

    public static DBResourceManager getInstance() {
        return INSTANCE;
    }

    public String getValue(String key) {
        return bundle.getString(key);
    }
}
