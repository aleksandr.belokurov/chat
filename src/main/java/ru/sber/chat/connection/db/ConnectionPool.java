package ru.sber.chat.connection.db;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ConnectionPool {
    private static final Logger LOGGER = Logger.getLogger(ConnectionPool.class);
    private static final ConnectionPool INSTANCE = createInstance();


    private BlockingQueue<Connection> connectionQueue;
    private BlockingQueue<Connection> givenAwayConQueue;

    private final String driverName;
    private final String url;
    private final String user;
    private final String password;
    private int poolSize;


    public ConnectionPool() {
        DBResourceManager dbResourceManager = DBResourceManager.getInstance();
        this.driverName = dbResourceManager.getValue(DBParameter.DB_DRIVER);
        this.url = dbResourceManager.getValue(DBParameter.DB_URL);
        this.user = dbResourceManager.getValue(DBParameter.DB_USER);
        this.password = dbResourceManager.getValue(DBParameter.DB_PASSWORD);
        try {
            this.poolSize = Integer.parseInt(dbResourceManager
                    .getValue(DBParameter.DB_POLL_SIZE));
        } catch (NumberFormatException e) {
            poolSize = 5;
        }
    }



    public static ConnectionPool getInstance() {
        return INSTANCE;
    }

    public void initPoolData() throws ConnectionPoolException {
        Locale.setDefault(Locale.ENGLISH);
        try {
            Class.forName(driverName);
            givenAwayConQueue = new ArrayBlockingQueue<>(poolSize);
            connectionQueue = new ArrayBlockingQueue<>(poolSize);
            for (int i = 0; i < poolSize; i++) {
                Connection connection = DriverManager.getConnection(url, user, password);
                PooledConnection pooledConnection = new PooledConnection(connection);
                connectionQueue.add(pooledConnection);
            }
        } catch (SQLException e) {
            throw new ConnectionPoolException("SQLException in ConnectionPool", e);
        } catch (ClassNotFoundException e) {
            throw new ConnectionPoolException("Can't find database driver class", e);
        }
    }

    public Connection getConnection() {
        Optional<Connection> connection = Optional.empty();
        try {
            connection = Optional.of(connectionQueue.take());
            givenAwayConQueue.add(connection.get());

        } catch (InterruptedException e) {
            LOGGER.log(Level.ERROR, "Interrupted!", e);
            Thread.currentThread().interrupt();
        }
        return connection.orElseThrow(NullPointerException::new);
    }

    public void freeConnection() {
        try {
            connectionQueue.add(givenAwayConQueue.take());

        } catch (InterruptedException e) {
            LOGGER.log(Level.ERROR, "Interrupted!", e);
            Thread.currentThread().interrupt();
        }
    }

    public void dispose() {
        clearConnectionQueue();
    }

    private void clearConnectionQueue() {
        try {
            closeConnectionsQueue(givenAwayConQueue);
            closeConnectionsQueue(connectionQueue);
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, "Error closing the connection.", e);
        }
    }

    public void closeConnection(Connection con, Statement st, ResultSet rs) {
        try {
            con.close();
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, "Connection isn't return to pool.");
        }

        try {
            rs.close();
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, "ResultSet isn't closed.");
        }

        try {
            st.close();
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, "Statement isn't closed.");
        }
        freeConnection();
    }

    public void closeConnection(Connection con, Statement st) {
        try {
            con.close();
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, "Connection isn't return to the pool.");
        }

        try {
            st.close();
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, "Statement isn't closed.");
        }
        freeConnection();
    }

    public void closeConnection(Connection con) {
        try {
            con.close();
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, "Connection isn't return to the pool.");
        }
        freeConnection();
    }

    private void closeConnectionsQueue(BlockingQueue<Connection> queue)
            throws SQLException {
        Connection connection;
        while ((connection = queue.poll()) != null) {
            if (!connection.getAutoCommit()) {
                connection.commit();
            }
            ((PooledConnection) connection).reallyClose();
        }
    }

    private static ConnectionPool createInstance() {
        ConnectionPool connectionPool = new ConnectionPool();
        try {
            connectionPool.initPoolData();
        } catch (ConnectionPoolException e) {
            LOGGER.error(e);
        }
        return connectionPool;
    }

}
