package ru.sber.chat.servlets;

import ru.sber.chat.commands.Command;
import ru.sber.chat.commands.CommandRuntimeException;
import ru.sber.chat.commands.Commandes;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Front controller implementation
 */
public class FrontController extends HttpServlet {


    private static final String CMD = "cmd";


    private static final long serialVersionUID = 1L;

    /**
     * Processing GET requests
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        new HttpController().redirect(req, resp);
    }

    /**
     * Processing POST requests
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            defineCommand(req.getParameter(CMD)).execute(req, resp);
        }catch (NullPointerException e) {
            new CommandRuntimeException().goToErrorPage(e, req, resp);
        }

    }

    /**
     * Define command by her name from request
     *
     * @param commandName
     * @return chosen command
     */
    protected Command defineCommand(String commandName) {
        return Commandes.valueOf(commandName).getCommand();
    }





}
