package ru.sber.chat.servlets;

import ru.sber.chat.commands.CommandRuntimeException;
import ru.sber.chat.datalayer.DBType;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HttpController {
    private static final String LOGIN = "login";
    private static final String REDIRECT = "redirect";
    private static final String CHAT = "chat";
    private static final String ERROR = "404 not found page chat";
    private static final String GET_ALL = "GET_ALL_LOGGED_USER";
    private static final String GET_MESSAGE = "GET_MESSAGE";
    private static final String USER_DAO = "userDao";
    private static final String PATH_LOGIN_JSP = "/WEB-INF/pages/login.jsp";
    private static final String PATH_CHAT_JSP = "/WEB-INF/pages/chat.jsp";
    private static final String SLASH = "/";


    public void redirect(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if (req.getAttribute(REDIRECT).equals(LOGIN)) {
            if (!req.getServletPath().equals(SLASH + LOGIN)) {
                resp.sendRedirect(LOGIN);
            } else {
                createLoginRequest(req, resp);
            }
        } else {
            if (!req.getServletPath().equals(SLASH + CHAT)) {
                resp.sendRedirect(CHAT);
            } else {
                createChatRequest(req, resp);
            }
        }
    }

    private void createLoginRequest(HttpServletRequest req,
                                    HttpServletResponse resp) {
        try {
            req.getRequestDispatcher(PATH_LOGIN_JSP).forward(req,
                    resp);
        } catch (ServletException | IOException e) {
            new CommandRuntimeException().goToErrorPage(e, req, resp);
        }
    }
    private void createChatRequest(HttpServletRequest req,
                                   HttpServletResponse resp) {
        req.getServletContext().setAttribute(USER_DAO,
                DBType.MYSQL.getDAOFactory().getUserDAO());
        FrontController frontController = new FrontController();
        frontController.defineCommand(GET_ALL).execute(req, resp);
        frontController.defineCommand(GET_MESSAGE).execute(req, resp);
        try {
            req.getRequestDispatcher(PATH_CHAT_JSP).forward(req,
                    resp);
        } catch (ServletException | IOException e) {
            new CommandRuntimeException().goToErrorPage(ERROR, req, resp);
        }
    }
}
