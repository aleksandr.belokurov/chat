<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
        }
    </style>
    <meta http-equiv="refresh" content="10">
    <title>Chat</title>
</head>
<body>
<div style="width: 80%; margin: 0 auto 0 auto;">
    <div>
        <span style="float: left;">Hello ${User.name}</span>
        <form action="" method="post">
            <input name="cmd" hidden="true" value="LOGOUT">
            <button type="submit" style="float: right;">Выход</button>
        </form>
    </div>
    <div style="width: 100%; overflow: hidden;">
        <div style="width: 80%; float: left;">
            Messages:
            <c:forEach var="message" items="${Messages}">
            <p>
                <span style="color: #0051dd">${message.date}</span> <span
                    style="color: #46cd41">${message.userFrom.name}</span>:<span>
                    ${message.message}</span>
                </c:forEach>
        </div>
        <div style="width: 20%; float: right;">
            Users:
            <c:forEach var="user" items="${Users}">
                <div style="width: 100%; overflow: hidden;">
                    <c:choose>
                        <c:when test="${User.role.toString() eq 'ADMIN'}">
                            <p style="float: left">
                                <a title="${user.role.getDescription() }"
                                   href=?linkeduser=${user.name}>${user.name}</a>
                            </p>
                            <form action="" method="post">
                                <c:if test="${user.role.toString() eq 'USER'}">
                                    <input name="nick" hidden="true" value="${user.name}">
                                    <c:choose>
                                        <c:when test="${userDao.isKicked(user)}">
                                            <input name="cmd" hidden="true" value="UNKICK">
                                            <button style="float: left">unkick</button>
                                        </c:when>
                                        <c:otherwise>
                                            <c:if test="${!userDao.isBlocked(user)}">
                                                <input name="cmd" value="BLOCK" title="block" type="submit">
                                            </c:if>
                                            <input name="cmd" value="KICK" title="kick" type="submit">
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                            </form>
                        </c:when>
                        <c:otherwise>
                            <p>${user.name}</p>
                        </c:otherwise>
                    </c:choose>
                </div>
            </c:forEach>
        </div>
    </div>

    <c:if test="${!userDao.isBlocked(User)}">
        <div style="width: 80%; bottom: 0; overflow: hidden; position: absolute;">
            <form action="" method="post" accept-charset="UTF-8">
                <input name="cmd" hidden="true" value="SEND_MESSAGE"> <input
                    value="${param.linkeduser}" name="messageContent" type="text"
                    style="width: 79%; height: 100%; float: left">
                <button style="width: 20%; height: 100%; float: left;">send</button>
            </form>
        </div>
    </c:if>

</div>
</body>
</html>
