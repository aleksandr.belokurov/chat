CREATE DATABASE chat;
USE chat;
CREATE TABLE role (
            id int not null primary key auto_increment,
            title varchar(100) not null unique,
            description text not null

);
CREATE TABLE status (
            id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
            title varchar(100) NOT NULL unique,
            description TEXT NOT NULL
);
CREATE TABLE user (
            nick VARCHAR(100) NOT NULL PRIMARY KEY,
            user_role INT NOT NULL,
            CONSTRAINT FK_USER_TO_ROLE FOREIGN KEY (user_role) REFERENCES role(ID)
);
CREATE INDEX INDEX_USER_TO_ROLE on user (user_role);
CREATE TABLE message (
            id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
            user_from VARCHAR(100) NOT NULL,
            message TEXT NOT NULL,
            time_stamp DATETIME NOT NULL ,
            status INT NOT NULL,
            CONSTRAINT FK_MESSAGE_TO_STATUS FOREIGN KEY (status) REFERENCES status(ID),
            CONSTRAINT FK_MESSAGE_TO_USER FOREIGN KEY (user_from) REFERENCES user(nick)
);
CREATE INDEX INDEX_MESSAGE_TO_STATUS on message (status);
CREATE INDEX INDEX_MESSAGE_TO_USER on message (user_from);