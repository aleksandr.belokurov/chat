use chat;

#add user
INSERT INTO user
VALUES ('ana', 1);

#check logged user
SELECT
    IF(status IN (2, 3), TRUE, FALSE) AS 'logged'
FROM message
WHERE chat.message.user_from = 'aleks'
ORDER BY ID DESC
LIMIT 1;

#check kicked user
SELECT
    EXISTS(
            SELECT *
            FROM message
            WHERE status = 4
              AND message = 'aleks'
        ) AS 'kicked';

#SELECT all logged user
SELECT
    user_logged.user_from,
    (SELECT title FROM chat.role WHERE id = chat.user.user_role)
FROM (
         SELECT
             user_from,
             status
         FROM (
                  SELECT
                      user_from,
                      status
                  FROM message
                  GROUP BY user_from, time_stamp
                  ORDER BY time_stamp DESC
              ) AS user
         GROUP BY user_from
     ) AS user_logged
         JOIN chat.user ON user_logged.user_from = chat.user.nick
WHERE user_logged.status IN (2, 3);

#SELECT all kicked user
SELECT
    user_from,
    user_role
FROM message
         JOIN user on nick = message.message
    AND message.status = 4
ORDER BY message;

#get user by nick
SELECT
    nick,
    user_role
FROM user
WHERE nick = 'never';

#get all user
SELECT
    nick,
    (SELECT role.title FROM role WHERE role.id = user.user_role)
FROM user
ORDER BY nick;

#get n message
SELECT
    user_from,
    time_stamp,
    message,
    (SELECT status.title FROM status WHERE status.id = message.status)
FROM (SELECT * FROM message ORDER BY time_stamp DESC LIMIT 2) AS message
ORDER BY time_stamp;

#create message
INSERT INTO message (user_from, message, time_stamp, status)
VALUES ('aleks', 'hello', NOW(), 1);

#delete message about kick
DELETE
FROM message
WHERE status = 4
  AND message = '';

#get role of user
SELECT
    role.title
FROM role
         JOIN user ON role.ID = user.user_role
WHERE user.nick = 'aleks';
