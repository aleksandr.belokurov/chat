use chat;
# Insert into status
INSERT INTO status (title, description)
VALUES ('LOGOUT', 'user logout of chat');
INSERT INTO status (title, description)
VALUES ('LOGIN', 'user login in chat');
INSERT INTO status (title, description)
VALUES ('MESSAGE', 'user send message');
INSERT INTO status (title, description)
VALUES ('KICK', 'admin kick other user');
INSERT INTO status (title, description)
VALUES ('BLOCK', 'admin block other user');


# Insert into role
INSERT INTO role (title, description)
VALUES ('ADMIN', 'admin role');
INSERT INTO role (title, description)
VALUES ('USER', 'user role');

# Insert into user
INSERT INTO user (nick, user_role)
VALUES ('aleks', '2');
INSERT INTO user (nick, user_role)
VALUES ('never', '1');
INSERT INTO user (nick, user_role)
VALUES ('nicky', '2');

# Insert into message
INSERT INTO message (user_from, message, time_stamp, status)
VALUES ('aleks', 'user login in chat', NOW(), 2);
INSERT INTO message (user_from, message, time_stamp, status)
VALUES ('never', 'user login in chat', NOW(), 2);
INSERT INTO message (user_from, message, time_stamp, status)
VALUES ('aleks', 'Hello', NOW(), 3);
INSERT INTO message (user_from, message, time_stamp, status)
VALUES ('never', 'aleks', NOW(), 4);
